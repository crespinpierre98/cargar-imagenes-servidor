import  { useState } from 'react';
import axios from 'axios';

// ...

const ImageUploader = () => {
    const [images, setImages] = useState([]);
    const [id, setId] = useState('');
    const [responseMessage, setResponseMessage] = useState('');
  
    const handleIdChange = (e) => {
      setId(e.target.value);
    };
  
    const handleImageChange = (e) => {
      const selectedFiles = e.target.files;
      // Convierte la colección de archivos a un array antes de establecerlo como estado
      setImages(Array.from(selectedFiles));
    };
  
    const handleUpload = async () => {
      try {
        const formData = new FormData();
        images.forEach((image, index) => {
          formData.append(`imagenes[${index}]`, image);
        });
        formData.append('id', id);
  
        const token = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmYWN1QGdtYWlsLmNvbSIsImlhdCI6MTcwMTk4NzcxMiwiZXhwIjoxNzAyMDc0MTEyfQ.WR7YZl1NU9pLyCPS6egH6LsXhV2xQPwKqtjU922Ub1w';
  
        const response = await axios.post('http://54.193.237.249:8090/imagen/upload', formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': `Bearer ${token}`,
          },
        });
  
        setResponseMessage(response.data);
      } catch (error) {
        console.error('Error al cargar imágenes:', error);
        setResponseMessage('Error al cargar imágenes');
      }
    };
  
  

  return (
    <div>
      <div>
        <label>ID:</label>
        <input type="text" value={id} onChange={handleIdChange} />
      </div>
      <div>
        <label>Seleccionar Imágenes:</label>
        <input type="file" multiple onChange={handleImageChange} />
      </div>
      <button onClick={handleUpload}>Subir Imágenes</button>
      {responseMessage && <p>{responseMessage}</p>}
    </div>
  );
};

export default ImageUploader;
